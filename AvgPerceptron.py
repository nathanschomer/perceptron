import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt
import pandas as pd


class AvgPerceptron:

    def __init__(self, train_file='./IA2-train.csv', dev_file='./IA2-dev.csv',
                 max_epochs=100, normalize=True, kaggle=False):

        self.max_epochs = max_epochs

        self.train_acc_norm = []
        self.train_acc_avg = []
        self.dev_acc_norm = []
        self.dev_acc_avg = []

        self.zero_idxs = None

        # load data
        self.train_data, self.train_labels = self.load_data(train_file,
                                                            normalize)
        self.dev_data, self.dev_labels = self.load_data(dev_file,
                                                        normalize,
                                                        use_saved_stats=True)
        self.feature_names = np.array(self.load_feature_names(train_file))
        self.num_features = self.train_data.shape[1]

        # initialize weights
        self.weights = None
        self.avg_weights = None
        self.reset_weights()

        # example counter
        self.s = 0

    def reset_weights(self, rand=None):
        self.weights = np.zeros((self.num_features, 1))
        self.avg_weights = np.zeros((self.num_features, 1))

    def load_feature_names(self, filename):
        with open(filename, 'r') as f:
            return f.readline().strip().split(',')

    def load_data(self, filename, normalize=False, use_saved_stats=False):
        """
        Load data from a specified CSV and return a numpy array
        """
        if isinstance(filename, list):
            raw_data = []
            for fn in filename:
                raw_data.append(pd.read_csv(fn))
            raw_data = pd.concat(raw_data)
        else:
            raw_data = pd.read_csv(filename)

        labels = None
        if 'Response' in raw_data.columns:
            labels = raw_data['Response'].to_numpy().reshape((-1, 1))
            raw_data.drop(columns='Response', inplace=True)

        norm_cols = ['Age', 'Vehicle_Age_0',
                     'Vehicle_Age_1', 'Vehicle_Age_2',
                     'Annual_Premium', 'Vintage']
        if normalize:

            if not use_saved_stats:
                self.norm_mean = raw_data[norm_cols].mean()
                self.norm_std = raw_data[norm_cols].std()

            raw_data[norm_cols] = (raw_data[norm_cols] -
                                   self.norm_mean) / self.norm_std

        data = raw_data.to_numpy()

        return data, labels

    def predict(self, weights, data):
        rslt = data @ weights
        rslt[rslt < 0] = 0
        rslt[rslt > 0] = 1
        return rslt

    def accuracy(self, weights, data, labels):
        out = self.predict(weights, data)
        correct = np.count_nonzero(np.equal(labels, out))
        return correct / labels.shape[0]

    def single_epoch(self):
        for x, y in zip(self.train_data, self.train_labels):
            y = y[0]
            if y * (x @ self.weights) <= 0:
                self.weights += (y * x).reshape(-1, 1)
            self.avg_weights = (self.s*self.avg_weights + self.weights) /\
                               (self.s+1)
            self.s += 1
        return

    def train(self):
        for n in tqdm(range(self.max_epochs)):
            self.single_epoch()
            self.epochs = n
            self.calc_accuracies()

    def calc_accuracies(self):

        # calc train & dev accuracies
        train_acc_norm = self.accuracy(self.weights,
                                       self.train_data,
                                       self.train_labels)
        train_acc_avg = self.accuracy(self.avg_weights,
                                      self.train_data,
                                      self.train_labels)
        dev_acc_norm = self.accuracy(self.weights,
                                     self.dev_data,
                                     self.dev_labels)
        dev_acc_avg = self.accuracy(self.avg_weights,
                                    self.dev_data,
                                    self.dev_labels)
        self.train_acc_norm.append(train_acc_norm)
        self.train_acc_avg.append(train_acc_avg)
        self.dev_acc_norm.append(dev_acc_norm)
        self.dev_acc_avg.append(dev_acc_avg)

    def plot_results(self):
        plt.plot(self.train_acc_norm)
        plt.plot(self.train_acc_avg)
        plt.plot(self.dev_acc_norm)
        plt.plot(self.dev_acc_avg)
        plt.legend(['Train Acc', 'Train Acc, Avg',
                    'Dev Acc', 'Dev Acc, Avg'])
        plt.show()


if __name__ == '__main__':

    ap = AvgPerceptron()
    ap.train()
    ap.plot_results()
